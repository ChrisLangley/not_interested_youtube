//Find every X button on youtube homepage labeled "Not Interested" and click it
function NotInterested() {
  document.querySelectorAll('[aria-label="Not interested"]').forEach(
    function (currentValue, currentIndex, listObj) {
      currentValue.click();
    }
  );
}
NotInterested();

//Every 5 seconds, make sure you have informed youtube that you are not interested
setInterval(function () { NotInterested() }, 5000);

